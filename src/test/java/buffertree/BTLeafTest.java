package buffertree;

import com.google.common.hash.BloomFilter;
import experiments.TestUtils;
import junit.framework.TestCase;
import org.apache.hadoop.hbase.KeyValue;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by t-guje on 9/12/2015.
 */
public class BTLeafTest extends TestCase {

    public void testUpdateBloomFilter() throws Exception {
        Random gen = new Random(123);
        KeyValue leaf_tuple = TestUtils.generateRandomKeyValue(gen);
        leaf_tuple = BTUtils.getLeafIndexTuple(leaf_tuple, 8);
        BloomFilter bf = BTLeaf.getBloomFilterFromTuple(leaf_tuple);
        System.out.println(leaf_tuple.getValue());
        ByteBuffer data = ByteBuffer.allocate(20000);
        KeyValue temp;
        Random gen0 = new Random(123);
        for(int i = 0; i < 200; i++) {
            temp = TestUtils.generateRandomKeyValue(gen0);
            data.put(temp.getBuffer(), temp.getOffset(), temp.getLength());
        }

        System.out.println(leaf_tuple.getLength());
        KeyValue modified_leaf_tuple = BTLeaf.updateBloomFilter(leaf_tuple, data, 0, data.position());
        BloomFilter modified_bf = BTLeaf.getBloomFilterFromTuple(modified_leaf_tuple);

        int neg_count = 0;
        Random gen1 = new Random(123);
        for(int i = 0; i < 199; i++) {
            temp = TestUtils.generateRandomKeyValue(gen1);
            if(!modified_bf.mightContain(temp.getKey())) {
                neg_count++;
            }
        }

        Random gen2 = new Random(321);
        System.out.println("False Negatives : " + neg_count);
        int pos_count = 0;
        for(int i = 0; i < 200; i++) {
            temp = TestUtils.generateRandomKeyValue(gen2);
            if(modified_bf.mightContain(temp.getKey())) {
                pos_count++;
            }
        }
        System.out.println("False Positives : " + pos_count);
    }
}