package buffertree;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import org.apache.hadoop.hbase.KeyValue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Stack;

public class BTLeaf {

	public static class Iterator implements BTUtils.Iterator {
		ByteBuffer leaf_page; 
		int index; 
		int max_index; 
		KeyValue current; 
		
		public Iterator(ByteBuffer leaf_page) {
			this.leaf_page = leaf_page; 
			this.index = BTPageHeader.startOfFreeSpaceDefault;
			this.max_index = BTPageHeader.getStartOfFreeSpace(leaf_page);
			
			this.current = BTUtils.getKeyValueAt(leaf_page, leaf_page.getInt(index)); 
			this.index += 4;
		}
		

		public boolean hasNext() {
			return (current != null); 
		}


		public KeyValue peek() {
			return current; 
		}

		public KeyValue next() {
			KeyValue ret = current;
			
			if(current == null) {
				return null;
			}
			
			if(index < max_index) {
				current = BTUtils.getKeyValueAt(leaf_page, leaf_page.getInt(index)); 
				index += 4;
			} else {
				current = null;
			}
			 
			return ret; 
		}
		
	}

	/**
	 * Inserts the passed on new_data into the file and splits the leaf if necessary. If the leaf is split, it returns 
	 * an array list of index tuples to be inserted into the parent node. Else, returns null.
	 * @param btree
	 * @param stack
	 * @param leaf_page
	 * @param new_data
	 * @param new_data_offset
	 * @param new_data_length
	 * @return
	 */
	public static ArrayList<KeyValue> insert(BTree btree, 
											 Stack<ByteBuffer> stack,
											 KeyValue indexTuple,
											 long leaf_page_id,
											 ByteBuffer leaf_page, 
											 ByteBuffer new_data, 
											 int new_data_offset, 
											 int new_data_length) {
		
		ArrayList<KeyValue> siblings = null; 
		
		int new_data_count = BTUtils.computeCount(new_data, new_data_offset, new_data_length);
		 
		if(BTPageHeader.getFreeSpace(leaf_page) > (new_data_length + 4*new_data_count)) {
			
			int endOfFreeSpace = BTPageHeader.getEndOfFreeSpace(leaf_page);
			int startOfFreeSpace = BTPageHeader.getStartOfFreeSpace(leaf_page);
			
			leaf_page.position(endOfFreeSpace - new_data_length); 
			leaf_page.put(new_data.array(), new_data_offset, new_data_length);
			
			int index_offset = startOfFreeSpace;
			int data_offset = endOfFreeSpace - new_data_length;
			while(data_offset < endOfFreeSpace){
				leaf_page.putInt(index_offset, data_offset);
				index_offset += 4; 
				data_offset += BTUtils.getKeyValueLengthAt(leaf_page, data_offset);
			}
				
			BTPageHeader.setEndOfFreeSpace(leaf_page, endOfFreeSpace - new_data_length);
			BTPageHeader.setStartOfFreeSpace(leaf_page, index_offset);
			
			BTPage.sortIndex(leaf_page);
			
			btree.writePage(leaf_page_id, leaf_page);
			indexTuple = BTLeaf.updateBloomFilter(indexTuple, new_data, new_data_offset, new_data_length);
		} else {
			
			siblings = splitInsert(btree, stack, indexTuple, leaf_page_id, leaf_page, new_data, new_data_offset, new_data_length, new_data_count);
			
		}
		
		return siblings;
	}
	
	//TODO writing onto file must be done
	/**
	 * Splits the leaf and inserts the passed on new_data into the leaf. It returns 
	 * an array list of index tuples to be inserted into the parent node. 
	 * @param btree
	 * @param stack
	 * @param leaf_page
	 * @param new_data
	 * @param new_data_offset
	 * @param new_data_length
	 * @param new_data_count
	 * @return
	 */
	public static ArrayList<KeyValue> splitInsert(BTree btree, 
												  Stack<ByteBuffer> stack,
												  KeyValue indexTuple,
												  long leaf_page_id,
												  ByteBuffer leaf_page, 
												  ByteBuffer new_data, 
												  int new_data_offset, 
												  int new_data_length, 
												  int new_data_count) {
		ArrayList<KeyValue> siblings = null;
		
		//find the number of leafs/nodes needed
		int per_page_capacity = BTPageHeader.getPageCapacity(leaf_page); 
		
		int total_count = new_data_count + BTPageHeader.getCount(leaf_page); 
		int data_size = new_data_length + BTPageHeader.getDataSize(leaf_page); 
		int index_size= total_count * 4; 
		int total_size = data_size + index_size; 
		
		int num_leafs = 2;
		int min_leaf_size = total_size/2;
		if( total_size >= 2 * per_page_capacity) {
			num_leafs = (int) Math.ceil((double)total_size / per_page_capacity);  
			min_leaf_size = (total_size / num_leafs);
		}
		
		//combine and find the split points
		int[] split_points = new int[num_leafs + 1];
		ByteBuffer combined = BTUtils.mergeForSplit(leaf_page, new_data, new_data_offset, new_data_length, min_leaf_size, split_points); 
		
		siblings = BTUtils.splitLeaf(btree, indexTuple, combined, split_points, leaf_page_id, leaf_page);
		return siblings;
	}
	
	//TODO new bloom filter
	/**
	 * Updates the bloom filter with the new data to be inserted
	 * @param leaf_tuple
	 * @param new_data
	 *
	 * @param offset
	 * @param length
	 * @return
	 */
	public static KeyValue updateBloomFilter(KeyValue leaf_tuple, ByteBuffer new_data, int offset, int length) {
		try {
			BloomFilter bf = getBloomFilterFromTuple(leaf_tuple);
			int curr_offset = offset;

			while(curr_offset < offset + length) {
				KeyValue temp = BTUtils.getKeyValueAt(new_data, curr_offset);
				bf.put(temp.getKey());
				curr_offset += temp.getLength();
			}

			KeyValue modified_leaf_tuple = writeBloomFilterToTuple(leaf_tuple, bf);
			return modified_leaf_tuple;
		} catch(Exception e) {
			System.err.println("Unable to serialize/deserialize bloom filter");
			System.exit(0);
		}
		return leaf_tuple;
	}

	public static BloomFilter getBloomFilterFromTuple(KeyValue leaf_tuple) throws IOException {
		int bfLength = leaf_tuple.getValueLength() - 8;
		int bfOffset = leaf_tuple.getValueOffset() + 8;
		ByteArrayInputStream in = new ByteArrayInputStream(leaf_tuple.getBuffer(), bfOffset, bfLength);
		BloomFilter bf = BloomFilter.readFrom(in, Funnels.byteArrayFunnel());
		return bf;
	}

	public static KeyValue writeBloomFilterToTuple(KeyValue leaf_tuple, BloomFilter bf) throws IOException {
		int bfLength = leaf_tuple.getValueLength() - 8;
		int bfOffset = leaf_tuple.getValueOffset() + 8;
		ByteArrayOutputStream out = new ByteArrayOutputStream(bfLength);
		bf.writeTo(out);
		System.arraycopy(out.toByteArray(), 0, leaf_tuple.getBuffer(), bfOffset,  bfLength);
		return leaf_tuple;
	}

}
