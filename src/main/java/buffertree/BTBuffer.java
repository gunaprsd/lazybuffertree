package buffertree;

import org.apache.hadoop.hbase.KeyValue;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Stack;

public class BTBuffer {
	
	
	public static class Iterator implements BTUtils.Iterator {
		/**
		 * Invariants: 
		 * 1. current always points to the value that has to be returned on invoking the next function.
		 * 2. offset is the offset of the next to next value ie.. the value that must be returned on invoking next the second time
		 * 3. If the iterator has reached the end, then the value of current is null.   
		 */
		ByteBuffer data; 
		int offset; 
		int max_offset; 
		KeyValue current;
		
		public Iterator(ByteBuffer buffer_page) {
			assert BTPageHeader.getPageType(buffer_page) == BTPageHeader.Type.buffer;
			
			this.data = buffer_page; 
			this.offset = BTPageHeader.startOfFreeSpaceDefault;
			this.max_offset = BTPageHeader.getStartOfFreeSpace(data);
			
			this.current = BTUtils.getKeyValueAt(data, offset);
			this.offset += current.getLength();
		}
		
		public Iterator(ByteBuffer data, int offset, int length) {
			this.data = data; 
			this.offset = offset;
			this.max_offset = offset + length;
			
			this.current = BTUtils.getKeyValueAt(data, offset);
			this.offset += current.getLength();
		}

		public boolean hasNext() { 
			return (current != null);
		}

		public KeyValue peek() {
			return current; 
		}

		public KeyValue next() {
			KeyValue ret = current;
			
			if(current == null) {
				return null;
			}
			
			if(offset < max_offset) {
				current = BTUtils.getKeyValueAt(data, offset);
				offset += current.getLength();
			} else {
				current = null;
			}
			return ret; 
		}
		
	}
	
	/**
	 * Inserts the passed on data into the buffer. Currently, it invokes empty buffer immediately.
	 * @param btree
	 * @param stack
	 * @param buffer_page
	 * @param new_data
	 * @param new_data_offset
	 * @param new_data_length
	 * @return
	 */
	public static ArrayList<KeyValue> insert(BTree btree,
											 Stack<ByteBuffer> stack,
											 long node_page_id,
											 long buffer_page_id,
											 ByteBuffer buffer_page, 
											 ByteBuffer new_data, 
											 int new_data_offset, 
											 int new_data_length,
											 boolean sync) {
		
		ArrayList<KeyValue> siblings = null; 
		 
		if(BTPageHeader.getFreeSpace(buffer_page) > new_data_length) {
			
			int startOfFreeSpace = BTPageHeader.getStartOfFreeSpace(buffer_page);
			buffer_page.position(startOfFreeSpace); 
			buffer_page.put(new_data.array(), new_data_offset, new_data_length);
			BTPageHeader.setStartOfFreeSpace(buffer_page, startOfFreeSpace + new_data_length);
			
			if(sync) {
				btree.writePage(buffer_page_id, buffer_page);
			}
			
		} else {
			ByteBuffer node_page = btree.readPage(node_page_id, BTPageHeader.Type.leaf_node); //can be any node!
			stack.push(node_page); //node pushed in
			ArrayList<KeyValue> children = empty(btree, stack, node_page_id, node_page, buffer_page_id, buffer_page, new_data, new_data_offset, new_data_length);
			stack.pop(); //node popped out
			
			if(children != null) {
				siblings = BTNode.insert(btree, stack, node_page_id, node_page, children);
			}
			
		}
		return siblings;
	}

	/**
	 * Includes the data passed on and empties the buffer
	 * @param btree
	 * @param stack
	 * @param buffer_page
	 * @param new_data
	 * @param new_data_offset
	 * @param new_data_length
	 * @return
	 */
	public static ArrayList<KeyValue> empty(BTree btree,
											Stack<ByteBuffer> stack,
											long node_page_id,
											ByteBuffer node_page,
											long buffer_page_id,
											ByteBuffer buffer_page, 
											ByteBuffer new_data, 
											int new_data_offset, 
											int new_data_length) {
		
	   ArrayList<KeyValue> children = null;
	   boolean isLeafNode = BTPageHeader.getPageType(node_page) == BTPageHeader.Type.leaf_node;
	   ByteBuffer combined_data = BTUtils.mergeAndSort(buffer_page, new_data, new_data_offset, new_data_length);
	   
	   
	   long child_page_id = -1, sink_page_id = -1;
	   ByteBuffer sink_page = null; 
	   int startOffset = 0, endOffset = 0;  
	   int nextIndexOffset = BTPageHeader.startOfFreeSpaceDefault;
	   int maxIndexOffset = BTPageHeader.getStartOfFreeSpace(node_page);
	   ArrayList<KeyValue> additional = null ;
	   KeyValue nextIndexTuple = null, indexTuple = null, highKey = null;
	   nextIndexTuple = BTUtils.getKeyValueAt(node_page, node_page.getInt(nextIndexOffset));
	   nextIndexOffset += 4;
	   
	   /*
	    * Invariants:
	    * indexOffset always points to the next index tuple
	    * endOffset < combined_data size
	    * highKey is key obtained from the next index tuple 
	    */
	   while(endOffset < combined_data.capacity()) {
		   startOffset = endOffset; 
		   
		   indexTuple = nextIndexTuple;
		   
		   //calculating end offset
		   if(nextIndexOffset < maxIndexOffset) {
			   nextIndexTuple = BTUtils.getKeyValueAt(node_page, node_page.getInt(nextIndexOffset));
			   highKey = BTUtils.getKeyFromIndex(nextIndexTuple);
			   KeyValue temp = BTUtils.getKeyValueAt(combined_data, startOffset);
			   while(KeyValue.COMPARATOR.compare(temp, highKey) < 0) {
				   endOffset += temp.getLength(); 
				   if(endOffset < combined_data.capacity()) {
					   temp = BTUtils.getKeyValueAt(combined_data, endOffset);
				   } else {
					   break;  
				   }
			   }
		   } else {
			   endOffset = combined_data.capacity();
		   }

		   child_page_id = BTUtils.getBlockIdFromIndex(indexTuple);
		   sink_page_id = isLeafNode ?  child_page_id : ((BTree.buffer_size == 0) ? -1 : child_page_id + BTree.buffer_offset);
		   sink_page = isLeafNode? btree.readPage(sink_page_id, BTPageHeader.Type.leaf) : btree.readPage(sink_page_id, BTPageHeader.Type.buffer);
		   
		   if(isLeafNode) {
			   //inserting data into the leaf
			   additional = BTLeaf.insert(btree, stack, indexTuple, sink_page_id, sink_page, combined_data, startOffset, endOffset - startOffset);
			   node_page.position(node_page.getInt(nextIndexOffset - 4));
			   node_page.put(indexTuple.getBuffer(), indexTuple.getOffset(), indexTuple.getLength());
		   } else {
			   additional = BTBuffer.insert(btree, stack, child_page_id, sink_page_id, sink_page, combined_data, startOffset, endOffset - startOffset, true);
		   }
		   
		   nextIndexOffset += 4;
		   
		   if(additional != null) {
			   if(children == null) {
				    children = new ArrayList<KeyValue>();
			   }
		       children.addAll(additional);
		   }
	   }
	   
	   //wait until all the stuff has been emptied!
	   cleanBuffer(buffer_page); 
	   if(BTree.buffer_size != 0)
		   btree.writePage(buffer_page_id, buffer_page);
		if(isLeafNode)
			btree.writePage(node_page_id, node_page);
	   return children;
	}

	public static void cleanBuffer(ByteBuffer page) {
		BTPageHeader.setStartOfFreeSpace(page, BTPageHeader.startOfFreeSpaceDefault);
	}
	
	public static ByteBuffer getBlankBufferPage(long node_blockId) {
		ByteBuffer buffer_page = ByteBuffer.allocate(BTree.buffer_size);
		BTPageHeader.setPageHeaderDataTemplate(buffer_page, BTPageHeader.Type.buffer, BTree.buffer_size, node_blockId, -1, -1);
		return buffer_page;
	}

	public static boolean verifyExistence(BTree tree, long buffer_page_id, ByteBuffer buffer_page, KeyValue kv) {
		int offset = BTPageHeader.startOfFreeSpaceDefault;
		int max_offset = BTPageHeader.getStartOfFreeSpace(buffer_page);
		KeyValue temp = null;
		while(offset < max_offset) {
			temp = BTUtils.getKeyValueAt(buffer_page, offset);
			if(KeyValue.COMPARATOR.compare(temp, kv) == 0) {
				return true;
			}
			offset += temp.getLength();
		}
		return false;
	}
}
