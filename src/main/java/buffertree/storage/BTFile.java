package buffertree.storage;

import buffertree.BTMeta;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

/**
 * 
 * Class BFile is used for accessing the buffer tree file from the disk
 * @author gunaprsd
 *
 */
public class BTFile implements IBTFile{
	String fileName;
	RandomAccessFile file;  
	
	public static BTFile getBTFile(String fileName) {
		BTFile newFile = new BTFile(fileName);
		newFile.open();
		return newFile;
	}
	
	public static BTFile getNewBTFile(String fileName,
									  long treeId,
									  int pageSize, 
									  int bufferSize) {
		BTFile bfile = new BTFile(fileName);
		bfile.open();
		ByteBuffer meta_page = ByteBuffer.allocate(pageSize);
		BTMeta.setMetaDataEmpty(meta_page, treeId, pageSize, bufferSize);
		bfile.write(0, meta_page);
		return bfile;
	}
	/**
	 * Constructor for BFile with a link to the tree and the file object. 
	 * 
	 * @param fileName
	 * 
	 */
	public BTFile(String fileName) {
		this.fileName = fileName;
	}

	public void open() {
		try {
			this.file = new RandomAccessFile(fileName, "rw");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads <i>size</i> amount of data from <i>offset</i> in the file
	 * 
	 *  @param offset file offset
	 *  @param size size to be read
	 *  @return newly created byte array of <i>size</i> bytes
	 */
	public ByteBuffer read(long offset, int size) {
		ByteBuffer page = null;
		byte[] data = null;
		try {
			file.seek(offset);
			data = new byte[size];
			file.read(data, 0, size);
			page = ByteBuffer.wrap(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return page;
	}

	public void read(long offset, int size, ByteBuffer page) {
		if (page.capacity() != size) throw new AssertionError();
		try {
			file.seek(offset);
			file.read(page.array(), page.arrayOffset(), page.limit());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Writes the data from <i>dataOffset</i> to <i>dataOffset + size</i> in the byte array onto the file at <i>offset</i>  
	 * @param offset file offset
	 */
	public void write(long offset, ByteBuffer page) {
		try {
			file.seek(offset);
			file.write(page.array(), 0, page.capacity());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Closes the file. This is mandatory for synchronizing with the disk. 
	 */
	public void close() {
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}
