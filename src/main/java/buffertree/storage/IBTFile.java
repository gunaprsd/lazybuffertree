package buffertree.storage;

import java.nio.ByteBuffer;

/**
 * Created by gunaprsd on 16/09/15.
 */
public interface IBTFile {

    public void open();

    public void read(long offset, int size, ByteBuffer page);

    public ByteBuffer read(long offset, int size);

    public void write(long offset, ByteBuffer page);

    public void close();
}
