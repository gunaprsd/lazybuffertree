package buffertree;

import org.apache.hadoop.hbase.KeyValue;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;

/**
 * Created by t-guje on 9/12/2015.
 */
public class BTPrint {

    public static void print_tree(BTree tree, String treeFileName) {
        long rootBlockId = BTMeta.getRootBlockId(tree.meta_page);

        PrintStream stdOutput = System.out;
        try {
            PrintStream filStream = new PrintStream(new FileOutputStream(treeFileName));
            System.setOut(filStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Root Buffer");
        print_node_buffer(tree.root_buffer_page);

        System.out.println("Root Node");
        print_node(tree, rootBlockId, tree.root_page);

        System.setOut(stdOutput);
    }
    public static void print_key_value_array(ByteBuffer page) {
        int offset = 0;
        System.out.println("-------------------");
        while(offset < page.capacity()) {
            KeyValue temp = BTUtils.getKeyValueAt(page, offset);
            System.out.println(temp);
            offset += temp.getLength();
        }
        System.out.println("-------------------");
        System.out.println();
    }

    public static void print_node_buffer(ByteBuffer page) {
        int offset = BTPageHeader.startOfFreeSpaceDefault;
        System.out.println("-------------------");
        while(offset < BTPageHeader.getStartOfFreeSpace(page)) {
            KeyValue temp = BTUtils.getKeyValueAt(page, offset);
            System.out.println(temp);
            offset += temp.getLength();
        }
        System.out.println("-------------------");
        System.out.println();
    }

    public static void print_node(BTree btree, long page_id, ByteBuffer page) {
        int cnt = BTPageHeader.getCount(page);
        if(BTPageHeader.getPageType(page) == BTPageHeader.Type.internal_node || BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf_node) {
            long[] childIds = new long[cnt];
            System.out.println("node id : " + page_id);
            System.out.println("-------------------");
            for(int i = 0; i < cnt; i++) {
                KeyValue indexTuple = BTUtils.getKeyValueAt(page, page.getInt(i * 4  + BTPageHeader.startOfFreeSpaceDefault));
                long blockId  = BTUtils.getBlockIdFromIndex(indexTuple);
                System.out.println(indexTuple + "   $$$$   "  + blockId);
                childIds[i] = blockId;
            }
            System.out.println("-------------------");
            System.out.println("Buffer");
            print_node_buffer(btree.readPage(page_id + BTree.buffer_offset, BTPageHeader.Type.buffer));

            for(int i = 0; i < cnt; i++) {
                if(BTPageHeader.getPageType(page) == BTPageHeader.Type.internal_node) {
                    print_node(btree, childIds[i], btree.readPage(childIds[i], BTPageHeader.Type.internal_node));
                } else {
                    print_node(btree, childIds[i], btree.readPage(childIds[i], BTPageHeader.Type.leaf));
                }
            }
        } else if(BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf) {
            System.out.println("leaf id : " + page_id);
            System.out.println("-------------------");
            for(int i = 0; i < cnt; i++) {
                KeyValue indexTuple = BTUtils.getKeyValueAt(page, page.getInt(i * 4  + BTPageHeader.startOfFreeSpaceDefault));
                System.out.println(indexTuple);
            }
            System.out.println("-------------------");
            System.out.println();
        }
    }
}
