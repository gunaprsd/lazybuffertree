package buffertree;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import com.google.common.primitives.Longs;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.KeyValue;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class BTUtils {

	public static interface Iterator {
		boolean hasNext();
		KeyValue next(); 
		KeyValue peek();
	}
	
	public static class ListIterator implements Iterator{
		int index; 
		ArrayList<KeyValue> list;
		KeyValue current;
		
		ListIterator(ArrayList<KeyValue> list) {
			this.list = list;
			this.current = list.get(0); 
			this.index = 1; 
		}

		public boolean hasNext() {
			return (current != null); 
		}

		public KeyValue next() {
			KeyValue ret = current;
			
			if(current == null) {
				return null;
			}
			
			if(index < list.size()) {
				current = list.get(index); 
				index++;
			} else {
				current = null;
			}
			 
			return ret; 
		}
		public KeyValue peek() {
			return current;
		}
	}
	
	/**
	 * Returns the key value in the page at the given offset
	 * @param page
	 * @param offset
	 * @return
	 */
	public static KeyValue getKeyValueAt(ByteBuffer page, int offset) {
 		int len = page.getInt(offset);
 		len += page.getInt(offset + 4);
 		KeyValue kv1 = new KeyValue(page.array(), offset, len+2*4);
 		return kv1;
 	}
	
	/**
	 * Returns the length of key value in the page at the given offset
	 * @param page
	 * @param offset
	 * @return
	 */
	public static int getKeyValueLengthAt(ByteBuffer page, int offset) {
		int len = 8;
		len += page.getInt(offset);
		len += page.getInt(offset + 4);
		return len;
	}
	
	/**
	 * Computes the number of key values in the passed data array
	 * @param data
	 * @param offset
	 * @param length
	 * @return
	 */
	public static int computeCount(ByteBuffer data, int offset, int length) {
		assert length > 0;
		int count = 0;
		int data_offset = offset; 
		while(data_offset < offset + length) {
			data_offset += getKeyValueLengthAt(data, data_offset); 
			count++; 
		}
		return count;
	}
	
	/**
	 * Computes the total size of all the key values in the array list
	 * @param tuples
	 * @return
	 */
	public static int computeSize(ArrayList<KeyValue> tuples) {
		int size = 0;
		for(int i = 0; i < tuples.size(); i++) {
			size += tuples.get(i).getLength();
		}
		return size;
	}
	
	/**
	 * Returns array (as ByteBuffer) of sorted key values using the page index
	 * @param page
	 * @return
	 */
	public static ByteBuffer getSortedKeyValues(ByteBuffer page) {
		int endOfFreeSpace = BTPageHeader.getEndOfFreeSpace(page);
		int size = BTPageHeader.getPageSize(page) - endOfFreeSpace;
		ByteBuffer buffer = ByteBuffer.allocate(size);
		
		int index_offset = BTPageHeader.startOfFreeSpaceDefault;
		int end_offset = BTPageHeader.getStartOfFreeSpace(page);
		while(index_offset < end_offset) {
			int data_offset = page.getInt(index_offset); 
			KeyValue temp = getKeyValueAt(page, data_offset);
			buffer.put(temp.getBuffer(), temp.getOffset(), temp.getLength());
			index_offset += 4; 
		}
		return buffer; 
	}
	
	/**
	 * merges the data in page and the data passed and returns the sorted array (as ByteBuffer)
	 * of key values
	 * @param page
	 * @param data
	 * @param offset
	 * @param length
	 * @return
	 */
	public static ByteBuffer mergeAndSort(ByteBuffer page, ByteBuffer data, int offset, int length) {
		int size = BTPageHeader.getDataSize(page);
		ByteBuffer merged_buffer = ByteBuffer.allocate(size + length); 
		merged_buffer.position(0);
		if(size > 0)
			merged_buffer.put(page.array(), BTPageHeader.startOfFreeSpaceDefault, size);
		merged_buffer.put(data.array(), offset, length); 
		return sort(merged_buffer);
	}
	
	/*public static ByteBuffer merge(ByteBuffer buf1, int offset1, int length1,
								   ByteBuffer buf2, int offset2, int length2) {
		
		ByteBuffer buf3 = ByteBuffer.allocate(length1 + length2); 
		buf3.position(0); 
 		
 		int pos1 = offset1, pos2 = offset2, pos3 = 0;
 		while(pos1 < (length1 + offset1) || pos2 < (offset2 + length2)) {
 			if(pos1 < (offset1 + length1) && pos2 < (offset2 + length2)) {
 				KeyValue kv1 = (KeyValue)getKeyValueAt(buf1, pos1);
 				KeyValue kv2 = (KeyValue)getKeyValueAt(buf2, pos2);
 				
 				buf1.position(pos1);
 	 			buf2.position(pos2);
 	 			buf3.position(pos3);
 	 			
 				int cmp = KeyValue.COMPARATOR.compare(kv1, kv2);
 				if(cmp <= 0) {
 					buf3.put(buf1.array(), pos1, kv1.getLength());
 					pos3 += kv1.getLength(); 
 					pos1 += kv1.getLength(); 
 				} else {
 					buf3.put(buf2.array(), pos2, kv2.getLength());
 					pos3 += kv2.getLength(); 
 					pos2 += kv2.getLength(); 
 				}
 			} else if(pos1 < (offset1 + length1)) {
 				buf3.put(buf1.array(), pos1, offset1 + length1 - pos1);
 				break;
 			} else {
 				buf3.put(buf2.array(), pos2, offset2 + length2 - pos2);
				break;
 			}
 		}
 		
 		return buf3;
	}*/
	
	/*public static ByteBuffer ImergeForSplit(ByteBuffer page, ByteBuffer data, int offset, int length, int min_leaf_size, int[] splitPoints) {

		ByteBuffer buf1 = getSortedKeyValues(page);
		int offset1 = 0;
		int length1 = buf1.capacity();
		
		ByteBuffer buf2 = data;
		int offset2 = offset; 
		int length2 = length; 
		
		ByteBuffer buf3 = ByteBuffer.allocate(length1 + length2);  
 		
 		int pos1 = offset1, pos2 = offset2, pos3 = 0;
 		int current_leaf_size = 0;
 		int split_point_index = 1; 
 		while(pos1 < (length1 + offset1) || pos2 < (offset2 + length2)) {
 			if(pos1 < (offset1 + length1) && pos2 < (offset2 + length2)) {
 				KeyValue kv1 = (KeyValue)getKeyValueAt(buf1, pos1);
 				KeyValue kv2 = (KeyValue)getKeyValueAt(buf2, pos2);
 				
 				buf1.position(pos1);
 	 			buf2.position(pos2);
 	 			buf3.position(pos3);
 	 			if(current_leaf_size > min_leaf_size) {
 	 				current_leaf_size = 0; 
 	 				splitPoints[split_point_index] = pos3; 
 	 				split_point_index++;
 	 			}
 	 			
 				int cmp = KeyValue.COMPARATOR.compare(kv1, kv2);
 				if(cmp <= 0) {
 					buf3.put(buf1.array(), pos1, kv1.getLength());
 					int kv_length = kv1.getLength(); 
 					pos3 += kv_length; 
 					pos1 += kv_length;
 					current_leaf_size += kv_length; 
 				} else {
 					buf3.put(buf2.array(), pos2, kv2.getLength());
 					int kv_length = kv2.getLength();
 					pos3 += kv_length; 
 					pos2 += kv_length; 
 					current_leaf_size += kv_length; 
 				}
 			} else if(pos1 < (offset1 + length1)) {
 				if(current_leaf_size > min_leaf_size) {
 	 				current_leaf_size = 0; 
 	 				splitPoints[split_point_index] = pos3; 
 	 				split_point_index++;
 	 			}
 				int len = Math.min(offset1 + length1 - pos1, min_leaf_size - current_leaf_size); 
 				buf3.put(buf1.array(), pos1, len);
 				pos1 += len;
 				pos3 += len;
 				current_leaf_size += len; 
 				
 			} else {
 				if(current_leaf_size > min_leaf_size) {
 	 				current_leaf_size = 0; 
 	 				splitPoints[split_point_index] = pos3; 
 	 				split_point_index++;
 	 			}
 				int len = Math.min(offset2 + length2 - pos2, min_leaf_size - current_leaf_size); 
 				buf3.put(buf2.array(), pos2, offset2 + length2 - pos2);
 				pos2 += len;
 				pos3 += len;
 				current_leaf_size += len; 
 			}
 		}
 		
 		assert split_point_index == splitPoints.length -2; 
 		return buf3;
	}*/
	
	/**
	 * Returns the KeyOnlyKeyValue from the given key value
	 * @param kv
	 * @return
	 */
	public static KeyValue getKeyFromIndex(KeyValue kv) {
		return new KeyValue(kv.getRow(), HConstants.LATEST_TIMESTAMP, KeyValue.Type.Minimum);
	}
	
	public static KeyValue getKeyOnly(KeyValue kv) {
		return new KeyValue(kv.getRow(), HConstants.LATEST_TIMESTAMP, KeyValue.Type.Minimum);
	}
	
	/**
	 * Returns the block Id from index tuple
	 * works both for leaf index tuple and node index tuple
	 * @param kv
	 * @return
	 */
	public static long getBlockIdFromIndex(KeyValue kv) {
		byte[] longs = new byte[Longs.BYTES];
		System.arraycopy(kv.getValue(), 0, longs, 0, Longs.BYTES);
		return Longs.fromByteArray(longs);
	}
	
	/**
	 * Sorts the passed ByteBuffer. First builds an index, sorts the index and relocates them
	 * @param buf
	 * @return
	 */
	public static ByteBuffer sort(ByteBuffer buf) {
		//building index
		ArrayList<Integer> index = buildIndex(buf);
		
		//sorting the indices according to key values 
		sortHelper(buf, index, 0, index.size());
		
		//relocating key values as per new index order 
		return relocateHelper(buf, index);
	}
	
	/**
	 * Builds the index on array of key values passed as ByteBuffer
	 * @param buf
	 * @return
	 */
	private static ArrayList<Integer> buildIndex(ByteBuffer buf) {
		ArrayList<Integer> index = new ArrayList<Integer>();
 		int pos = buf.arrayOffset();
 		buf.position(pos); 
 		
 		while(buf.hasRemaining()) {
			//storing index
			index.add(pos);

			//skipping key value
			int keylen = buf.getInt(pos); pos += 4; 
			int vallen = buf.getInt(pos); pos += 4;
			pos += keylen + vallen;
			
			buf.position(pos);
		}
 		
 		return index;
 	}
	
	/**
	 * Sort step of merge sort
	 * @param buf
	 * @param index
	 * @param start
	 * @param end
	 */
 	private static void sortHelper(ByteBuffer buf, ArrayList<Integer> index, int start, int end) {
 		int mid = (start + end)/2;
 		if(start >= end -1) 
 			return;
 		
 		//sort the halves
 		sortHelper(buf, index, start, mid);
 		sortHelper(buf, index, mid, end);
 		
 		//merge them
 		mergeHelper(buf, index, start, mid, end);
 	}
 	
 	/**
 	 * Merge step in merge sort
 	 * @param buf
 	 * @param index
 	 * @param start
 	 * @param mid
 	 * @param end
 	 */
 	private static void mergeHelper(ByteBuffer buf, ArrayList<Integer> index, int start, int mid, int end) {
 		int i = start, j = mid; 
 		int[] newIndex = new int[end - start]; 
 		int k = 0; 
 		
 		while(i < mid || j < end) {
 			if(i < mid && j < end) {
 				KeyValue kv1 = (KeyValue)getKeyValueAt(buf, index.get(i));
 				KeyValue kv2 = (KeyValue)getKeyValueAt(buf, index.get(j));
 				
 				int cmp = KeyValue.COMPARATOR.compare(kv1, kv2);
 				if(cmp <= 0) {
 					newIndex[k] = index.get(i);
 					i++;
 				} else {
 					newIndex[k] = index.get(j);
 					j++;
 				}
 			} else if(i < mid) {
 				newIndex[k] = index.get(i);
				i++;
 			} else {
 				newIndex[k] = index.get(j);
				j++;
 			}
 			k++;
 		}
 		
 		//copying back to the array
 		for(int t = 0; t < end - start; t++) {
 			index.set(start+t, newIndex[t]); 
 		}
 		
 		//explicit signal to delete the array
 		newIndex = null; 
 	}
 	
 	/**
 	 * relocates data according to the sorted indices so that the key values are sorted physically
 	 * @param buf
 	 * @param index
 	 * @return
 	 */
 	private static ByteBuffer relocateHelper(ByteBuffer buf, ArrayList<Integer> index) {
 		ByteBuffer newBuf = ByteBuffer.allocate(buf.capacity()); 
 		
 		int newPos = 0;
 		//dumping relocated array in a new buffer
 		newBuf.position(newPos);
 		for(int i = 0; i < index.size(); i++) {
 			int len = ((KeyValue)getKeyValueAt(buf, index.get(i))).getLength();
 			newBuf.position(newPos);
 			newBuf.put(buf.array(), index.get(i), len);
 			newPos += len; 
 		}
 		
 		//return 
 		return newBuf;
 	}
	
/*	public static ByteBuffer mergeForSplit(ByteBuffer buf1, int offset1, int length1, ArrayList<KeyValue> tuples, int size, int min_leaf_size, int[] splitPoints) {
		int index = 0;  
		
		ByteBuffer buf3 = ByteBuffer.allocate(length1 + size);  
 		
 		int pos1 = offset1, pos3 = 0;
 		int current_leaf_size = 0;
 		int split_point_index = 1; 
 		while(pos1 < (length1 + offset1) || index < tuples.size()) {
 			if(current_leaf_size > min_leaf_size) {
 				current_leaf_size = 0; 
 				splitPoints[split_point_index] = pos3; 
 				split_point_index++;
 			}
 			
 			if(pos1 < (offset1 + length1) && index < tuples.size()) {
 				KeyValue kv1 = (KeyValue)getKeyValueAt(buf1, pos1);
 				KeyValue kv2 = tuples.get(index);
 				
 				buf1.position(pos1);
 	 			buf3.position(pos3);
 	 			
 				int cmp = KeyValue.COMPARATOR.compare(kv1, kv2);
 				if(cmp <= 0) {
 					buf3.put(buf1.array(), pos1, kv1.getLength());
 					int kv_length = kv1.getLength(); 
 					pos3 += kv_length; 
 					pos1 += kv_length;
 					current_leaf_size += kv_length; 
 				} else {
 					buf3.put(kv2.getBuffer(), kv2.getOffset(), kv2.getLength());
 					int kv_length = kv2.getLength();
 					pos3 += kv_length; 
 					index++;
 					current_leaf_size += kv_length; 
 				}
 			} else if(pos1 < (offset1 + length1)) {
 				int len = Math.min(offset1 + length1 - pos1, min_leaf_size - current_leaf_size); 
 				buf3.put(buf1.array(), pos1, len);
 				pos1 += len;
 				pos3 += len;
 				current_leaf_size += len; 
 				
 			} else {
 				KeyValue kv2 = tuples.get(index); 
 				buf3.put(kv2.getBuffer(), kv2.getOffset(), kv2.getLength());
				int kv_length = kv2.getLength();
				pos3 += kv_length; 
				index++;
 				current_leaf_size += kv_length; 
 			}
 		}
		
		return null;
	}*/

 	/**
 	 * Merges the key values in the page (node or leaf) and the passed data and returns a sorted ByteBuffer of all key values. 
 	 * It also stores the split points in the ByteBuffer to use for splitting. 
 	 * Note: the array of split points include the boundary points: 0 and full_size
 	 * @param page
 	 * @param data
 	 * @param offset
 	 * @param length
 	 * @param min_split_size
 	 * @param split_points
 	 * @return
 	 */
	public static ByteBuffer mergeForSplit(ByteBuffer page, ByteBuffer data, int offset, int length, int min_split_size, int[] split_points) {
		int size = length + BTPageHeader.getDataSize(page);
		
		BTUtils.Iterator iter1 = null;
		if(BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf) {
			iter1 = new BTLeaf.Iterator(page);
		} else if(BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf_node ||
				BTPageHeader.getPageType(page) == BTPageHeader.Type.internal_node) {
			iter1 = new BTNode.Iterator(page);
		}

		BTUtils.Iterator iter2 = new BTBuffer.Iterator(data, offset, length);

		return mergeForSplit(iter1, iter2, size, min_split_size, split_points);
	}

	/**
	 * Merges the key values in the page (node or leaf) and the list of key values and returns a sorted ByteBuffer of all key values.
 	 * It also stores the split points in the ByteBuffer to use for splitting.
 	 * Note: the array of split points include the boundary points: 0 and full_size
	 * @param page
	 * @param tuples
	 * @param tuples_size
	 * @param min_split_size
	 * @param split_points
	 * @return
	 */
	public static ByteBuffer mergeForSplit(ByteBuffer page, ArrayList<KeyValue> tuples, int tuples_size, int min_split_size, int[] split_points) {
		int size = tuples_size + BTPageHeader.getDataSize(page);

		BTUtils.Iterator iter1 = null;
		if(BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf) {
			iter1 = new BTLeaf.Iterator(page);
		} else if(BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf_node ||
				BTPageHeader.getPageType(page) == BTPageHeader.Type.internal_node) {
			iter1 = new BTNode.Iterator(page);
		}

		BTUtils.Iterator iter2 = new BTUtils.ListIterator(tuples);
		
		return mergeForSplit(iter1, iter2, size, min_split_size, split_points);
	}
	
	/**
	 * Merges the key values in the two iterators and returns a sorted ByteBuffer of all key values. 
 	 * It also stores the split points in the ByteBuffer to use for splitting. 
 	 * Note: the array of split points include the boundary points: 0 and full_size
	 * @param iter1
	 * @param iter2
	 * @param size
	 * @param min_split_size
	 * @param split_points
	 * @return
	 */
	public static ByteBuffer mergeForSplit(Iterator iter1, Iterator iter2, int size, int min_split_size, int [] split_points) {
		int current_size = 0; 
		
		split_points[0] = 0; 
		split_points[split_points.length -1] = size;  
		int split_points_index = 1; 
		ByteBuffer buffer = ByteBuffer.allocate(size);
		buffer.position(0);
		KeyValue kv1 = null, kv2 = null; 
		
		while(iter1.hasNext() || iter2.hasNext()) {
			
			if(current_size > min_split_size) {
				split_points[split_points_index] = current_size;
				current_size = 0; 
				split_points_index++;
			}
			
			kv1 = iter1.peek();
			kv2 = iter2.peek(); 
			
			int cmp = (kv1 == null)? 1 : (kv2 == null)? -1 : KeyValue.COMPARATOR.compare(kv1, kv2);
			if(cmp <= 0) {
				buffer.put(kv1.getBuffer(), kv1.getOffset(), kv1.getLength());
				iter1.next(); 
				current_size += kv1.getLength();
			} else {
				buffer.put(kv2.getBuffer(), kv2.getOffset(), kv2.getLength());
				iter2.next(); 
				current_size += kv2.getLength();
			}
		}
		
		return buffer;
	}

	public static KeyValue getLowestKeyIndexTuple(long blockId) {
		KeyValue kv = KeyValue.LOWESTKEY;
		ByteBuffer value = ByteBuffer.allocate(8); 
		value.putLong(0, blockId);
		return new KeyValue(HConstants.EMPTY_BYTE_ARRAY, HConstants.EMPTY_BYTE_ARRAY, HConstants.EMPTY_BYTE_ARRAY, value.array());
	}

	public static KeyValue getIndexTuple(KeyValue kv, long val) {
		ByteBuffer value = ByteBuffer.allocate(Longs.BYTES);
		value.putLong(val);
		return new KeyValue(kv.getRow(), kv.getFamily(), kv.getQualifier(), value.array());
	}

	public static KeyValue getLeafIndexTuple(KeyValue kv, long val) {
		try {
			BloomFilter bf = BloomFilter.create(Funnels.byteArrayFunnel(), 150, 0.1);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			bf.writeTo(out);
			ByteBuffer value = ByteBuffer.allocate(Longs.BYTES + out.size());
			value.putLong(0, val);
			value.position(Longs.BYTES);
			value.put(out.toByteArray());
			return new KeyValue(kv.getRow(), kv.getFamily(), kv.getQualifier(), value.array());
		} catch (Exception e) {
		}
		return null;
	}

	public static ArrayList<KeyValue> splitLeaf(BTree btree, KeyValue indexTuple, ByteBuffer data, int[] split_points, long page_id, ByteBuffer page) {
		ArrayList<KeyValue> siblings = new ArrayList<KeyValue>();
		int num_pages = split_points.length - 1;
		int size = BTPageHeader.getPageSize(page);

		long finalNextBlockId = BTPageHeader.getNextPageBlkNum(page);
		//KeyValue finalHighKey = BTPageHeader.getHighKey(page);

		ByteBuffer cpage = page;
		long cpage_id = page_id;
		KeyValue cIndexTuple = null;

		long next_page_id = -1;
		KeyValue next_page_low_key = null;

		BTPageHeader.cleanData(cpage);

		for(int i = 1; i <= num_pages; i++) {

			//insert appropriate stuff
			int coffset = split_points[i-1];
			int clength = split_points[i] - split_points[i-1];

			int endOfFreeSpace = BTPageHeader.getPageSize(cpage);
			cpage.position(endOfFreeSpace - clength);
			cpage.put(data.array(), coffset, clength);
			BTPageHeader.setEndOfFreeSpace(cpage, endOfFreeSpace - clength);
			BTPage.rebuildIndex(cpage);


			//Updating the bloom filters accordingly
			if(i == 1) {
				cIndexTuple = BTUtils.getLeafIndexTuple(indexTuple, page_id);
				cIndexTuple = BTLeaf.updateBloomFilter(cIndexTuple, data, coffset, clength);
				System.arraycopy(cIndexTuple.getBuffer(), cIndexTuple.getOffset(), indexTuple.getBuffer(), indexTuple.getOffset(), indexTuple.getLength());
			} else {
				cIndexTuple = BTLeaf.updateBloomFilter(cIndexTuple, data, coffset, clength);
				siblings.add(cIndexTuple);
			}

			if(i != num_pages) {
				next_page_low_key = BTUtils.getKeyOnly(BTUtils.getKeyValueAt(data, split_points[i]));
				next_page_id = btree.getNewBlockId();

				//link the new page to current page
				BTPageHeader.setNextPageBlkNum(cpage, next_page_id);
				//Writing onto file
				btree.writePage(cpage_id, cpage);

				//Stuff for the next iteration
				cpage = ByteBuffer.allocate(size);
				cpage_id = next_page_id;
				cIndexTuple = BTUtils.getLeafIndexTuple(next_page_low_key, next_page_id);

				BTPageHeader.setPageHeaderDataTemplate(cpage, BTPageHeader.getPageType(page), size, -1, -1, -1);
				BTPageHeader.setPrevPageBlkNum(cpage, -1);

			} else {
				//For the last page
				BTPageHeader.setNextPageBlkNum(cpage, finalNextBlockId);
				//Writing onto file
				btree.writePage(cpage_id, cpage);
			}
		}
		return siblings;
	}

	public static ArrayList<KeyValue> splitNode(BTree btree, ByteBuffer data, int[] split_points, long page_id, ByteBuffer page) {
		ArrayList<KeyValue> siblings = new ArrayList<KeyValue>(); 
		int num_pages = split_points.length - 1;
		int size = BTPageHeader.getPageSize(page);
		
		long finalNextBlockId = BTPageHeader.getNextPageBlkNum(page);
		//KeyValue finalHighKey = BTPageHeader.getHighKey(page);
		
		ByteBuffer cpage = page;
		long cpage_id = page_id;
		KeyValue cIndexTuple = null;

		long next_page_id = -1;
		KeyValue next_page_low_key = null;

		BTPageHeader.cleanData(cpage);
		
		for(int i = 1; i <= num_pages; i++) {
			
			//insert appropriate stuff
			int coffset = split_points[i-1]; 
			int clength = split_points[i] - split_points[i-1];
			
			int endOfFreeSpace = BTPageHeader.getPageSize(cpage);
			cpage.position(endOfFreeSpace - clength); 
			cpage.put(data.array(), coffset, clength);
			BTPageHeader.setEndOfFreeSpace(cpage, endOfFreeSpace - clength);
			BTPage.rebuildIndex(cpage);
			
			if(i != num_pages) {
				next_page_low_key = BTUtils.getKeyOnly(BTUtils.getKeyValueAt(data, split_points[i]));
				next_page_id = btree.getNewBlockId();

				//link the new page to current page
				BTPageHeader.setNextPageBlkNum(cpage, next_page_id);
				
				//Writing onto file
				btree.writePage(cpage_id, cpage);
				if(i != 1) {
					btree.writePage(cpage_id + BTree.buffer_offset, BTBuffer.getBlankBufferPage(cpage_id));
				}

				cpage = ByteBuffer.allocate(size);
				cpage_id = next_page_id;
				cIndexTuple = BTUtils.getIndexTuple(next_page_low_key, next_page_id);;
				siblings.add(cIndexTuple);

				BTPageHeader.setPageHeaderDataTemplate(cpage, BTPageHeader.getPageType(page), size, -1, -1, -1);
				BTPageHeader.setPrevPageBlkNum(cpage, -1);
			} else {
				//For the last page
				BTPageHeader.setNextPageBlkNum(cpage, finalNextBlockId);
				//Writing onto file
				btree.writePage(cpage_id, cpage);
				if(i != 1) {
					btree.writePage(cpage_id + BTree.buffer_offset, BTBuffer.getBlankBufferPage(cpage_id));
				}
			}
		}
		
		return siblings;
	}
	
	public static KeyValue getLowKey(ByteBuffer page) {
		int offset = page.getInt(BTPageHeader.startOfFreeSpaceDefault);
		return getKeyValueAt(page, offset);
	}
}
