package buffertree;

import buffertree.scanner.BTNodeScanner;
import buffertree.scanner.KeyValueScanner;
import buffertree.storage.BTFile;
import org.apache.hadoop.hbase.KeyValue;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Stack;

public class BTree {
	
	public static int page_size = 4096;
	public static int num_leaf_pages = 2; 
	public static int num_buffer_pages = 0;
	public static int num_node_pages = num_leaf_pages - num_buffer_pages; 
	
	public static int leaf_size = num_leaf_pages * page_size; 
	public static int node_size = num_node_pages * page_size; 
	public static int buffer_size = num_buffer_pages * page_size;
	
	public static int buffer_offset = node_size / page_size; 
	
	public static long meta_blockId = 0;
	public static long root_buffer_blockId = 1; 
	
	public String file_name; 
	public ByteBuffer meta_page;
	public ByteBuffer root_page;
	public ByteBuffer root_buffer_page;
	
	public BTFile file;
	
	public BTree(String file_name, boolean new_tree) {
		this.file_name = file_name;
		if(new_tree) {
			file = BTFile.getNewBTFile(file_name, 1234, page_size, buffer_size);
			file.open();
			meta_page = file.read(0, page_size);
			
			long leafBlockId = getNewBlockId();
			ByteBuffer leaf = ByteBuffer.allocate(leaf_size);
			BTPageHeader.setPageHeaderDataTemplate(leaf, BTPageHeader.Type.leaf, leaf_size, -1, -1, -1);
			writePage(leafBlockId, leaf);
			KeyValue indexTuple = BTUtils.getLowestKeyIndexTuple(leafBlockId);
			KeyValue indexLeafTuple = BTUtils.getLeafIndexTuple(indexTuple, leafBlockId);
			
			long rootBlockId = getNewBlockId();
			root_page = ByteBuffer.allocate(node_size);
			BTPageHeader.setPageHeaderDataTemplate(root_page, BTPageHeader.Type.leaf_node, node_size, -1, -1, -1);
			BTPage.insert(root_page, indexLeafTuple);
			writePage(rootBlockId, root_page);
			writePage(rootBlockId + BTree.buffer_offset, BTBuffer.getBlankBufferPage(rootBlockId));
			
			root_buffer_page = ByteBuffer.allocate(buffer_size);
			BTPageHeader.setPageHeaderDataTemplate(root_buffer_page, BTPageHeader.Type.buffer, buffer_size, rootBlockId, -1, -1);
			writePage(root_buffer_blockId, root_buffer_page);
			
			setRootBlockId(rootBlockId);
		} else {
			file = BTFile.getBTFile(file_name);
			meta_page = file.read(0, page_size);
			
			long rootBlockId = BTMeta.getRootBlockId(meta_page);
			root_page = readPage(rootBlockId, BTPageHeader.Type.internal_node);

			root_buffer_page = readPage(root_buffer_blockId, BTPageHeader.Type.buffer);
		}
	}
	
	public void close(boolean print) {
		writePage(meta_blockId, meta_page);
		long rootBlockId = BTMeta.getRootBlockId(meta_page);
		writePage(rootBlockId, root_page);
		writePage(root_buffer_blockId, root_buffer_page);

		if(print) {
			BTPrint.print_tree(this, "tree_trace.dat");
		}
		file.close();
	}

	public void close() {
		close(false);
	}
	
	public void insert(KeyValue kv) {
		ArrayList<KeyValue> siblings = null; 
		Stack<ByteBuffer> stack = new Stack<ByteBuffer>(); 
		
		ByteBuffer data = ByteBuffer.wrap(kv.getBuffer());
		long rootBlockId = BTMeta.getRootBlockId(meta_page);
		siblings = BTBuffer.insert(this, stack, rootBlockId, root_buffer_blockId, root_buffer_page, data, kv.getOffset(), kv.getLength(), false);
		root_page = readPage(rootBlockId, BTPageHeader.Type.internal_node);
		
		if(siblings != null) {
			ByteBuffer prev_root = root_page;
			long prev_root_blockId = BTMeta.getRootBlockId(meta_page);
			KeyValue indexTuple = BTUtils.getIndexTuple(BTUtils.getLowKey(prev_root), prev_root_blockId); 
			
			rootBlockId = getNewBlockId();
			root_page = ByteBuffer.allocate(node_size);
			BTPageHeader.setPageHeaderDataTemplate(root_page, BTPageHeader.Type.internal_node, node_size, -1, -1, -1);
			BTPage.insert(root_page, indexTuple);
			for(int i = 0; i < siblings.size(); i++) {
				BTPage.insertSorted(root_page, siblings.get(i));
			}
			BTPageHeader.setNodeBlkNum(root_buffer_page, rootBlockId);
			setRootBlockId(rootBlockId);
			
			writePage(rootBlockId, root_page);
			writePage(rootBlockId + BTree.buffer_offset, BTBuffer.getBlankBufferPage(rootBlockId));
		}
	}

	public boolean verifyInsert(KeyValue kv) {
		if(verifyExistence(kv)) {
			return false;
		} else {
			insert(kv);
			return true;
		}
	}

	public boolean verifyExistence(KeyValue kv) {
		boolean isInRootBuffer = BTBuffer.verifyExistence(this, -1, root_buffer_page, kv);
		long rootBlockId = BTMeta.getRootBlockId(meta_page);
		boolean isInTree = BTNode.verifyExistence(this, rootBlockId, root_page, kv);
		return isInRootBuffer || isInTree;
	}

	public KeyValueScanner getScanner(KeyValue startKey, KeyValue stopKey) {
		long rootBlockId = BTMeta.getRootBlockId(meta_page);
		KeyValueScanner scanner = new BTNodeScanner(this, rootBlockId, root_page, -1, root_buffer_page, startKey, stopKey);
		return scanner;
	}

	public KeyValueScanner getKeyValue(KeyValue scanKey) {
		KeyValue startKey = KeyValue.createFirstOnRow(scanKey.getRow());
		KeyValue stopKey = KeyValue.createLastOnRow(scanKey.getRow());
		return getScanner(startKey, stopKey);
	}
	
	public ByteBuffer readPage(long blockId, BTPageHeader.Type type) {
		if(type == BTPageHeader.Type.buffer) {
			if(buffer_size == 0)
				return ByteBuffer.allocate(0);
			return file.read(blockId * page_size, buffer_size); 
		} else if(type == BTPageHeader.Type.leaf) {
			return file.read(blockId * page_size, leaf_size); 
		} else {
			return file.read(blockId * page_size, node_size);
		}
	}
	
	public void writePage(long blockId, ByteBuffer page) {
		file.write(blockId * page_size, page);
	}

	public void setRootBlockId(long rootBlockId) {
		BTMeta.setRootBlockId(meta_page, rootBlockId);
		writePage(meta_blockId, meta_page);
	}

	public long getNewBlockId() {
		long new_blockId = BTMeta.getNewBlockId(meta_page);
		return  new_blockId;
	}
}
