package buffertree;

import java.nio.ByteBuffer;

public class BTMeta {
	/*
	 * Meta page design
	 * Tree Id : 8 bytes
	 * Root node block Id : 8 bytes
	 * Last allotted block Id : 8 bytes
	 * 
	 * Leaf/Node size : 4 bytes
	 * Buffer size: 4 bytes
	 * 
	 */
	
	public static long dataStartBlockId = 16;
	
	public static void setMetaDataEmpty(ByteBuffer page,
									   long treeId,
									   int pageSize, 
									   int bufferSize) {
		page.position(0);
		page.putLong(treeId);
		page.putLong(-1);
		page.putLong(dataStartBlockId);
		page.putInt(pageSize);
		page.putInt(bufferSize);
	}
	
	public static void setMetaData(ByteBuffer page,
								   long treeId,
								   long rootNodeBlockId,
								   long lastAllottedBlockId,
								   int pageSize,
								   int bufferSize) {
		page.position(0);
		page.putLong(treeId);
		page.putLong(rootNodeBlockId);
		page.putLong(lastAllottedBlockId);
		page.putInt(pageSize);
		page.putInt(bufferSize);
	}

	public static void setRootBlockId(ByteBuffer page, long rootBlockId) {
		page.putLong(8, rootBlockId);
	}
	
	public static long getRootBlockId(ByteBuffer page) {
		return page.getLong(8);
	}
	
	public static long getNewBlockId(ByteBuffer page) {
		long lastAllocatedId = page.getLong(16);
		lastAllocatedId += 2;
		page.putLong(16, lastAllocatedId);
		return lastAllocatedId; 
	}
	public static long getLastAllocatedBlockId(ByteBuffer page) {
		return page.getLong(16);
	}
	
	public static void setLastAllocatedBlockId(ByteBuffer page, long lastAllocatedBlockId) {
		page.putLong(16, lastAllocatedBlockId);
	}
}
