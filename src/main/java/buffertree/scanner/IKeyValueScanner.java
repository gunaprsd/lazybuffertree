package buffertree.scanner;


import org.apache.hadoop.hbase.KeyValue;

/**
 * Created by t-guje on 9/11/2015.
 */
public interface IKeyValueScanner {

    public KeyValue peek();

    public KeyValue next();
}
