package buffertree.scanner;

import buffertree.BTPage;
import buffertree.BTPageHeader;
import buffertree.BTUtils;
import buffertree.BTree;
import org.apache.hadoop.hbase.KeyValue;

import java.nio.ByteBuffer;

/**
 * Created by t-guje on 9/11/2015.
 */
public class BTLeafScanner implements IKeyValueScanner{

    BTree tree;
    KeyValue startKey;
    KeyValue stopKey;

    long page_id;
    ByteBuffer leaf_page;
    int max_offset;
    int next_offset;
    KeyValue current;

    public BTLeafScanner(BTree tree, long page_id, KeyValue startKey, KeyValue stopKey) {
        this.startKey = startKey;
        this.stopKey = stopKey;
        this.tree = tree;
        this.page_id = page_id;
        this.leaf_page = tree.readPage(page_id, BTPageHeader.Type.leaf);
        this.next_offset = BTPage.binSearchGreaterThanEquals(leaf_page, startKey);
        if(this.next_offset == -1) {
            this.next_offset = BTPageHeader.getStartOfFreeSpace(leaf_page);
        }
        this.max_offset = BTPage.binSearchGreaterThan(leaf_page, stopKey);
        if(this.max_offset == -1) {
            this.max_offset = BTPageHeader.getStartOfFreeSpace(leaf_page);
        }
        next();
    }

    @Override
    public KeyValue peek() {
        return current;
    }

    @Override
    public KeyValue next() {
        boolean scanner_done = (current == null) && (next_offset >= max_offset);
        if(scanner_done) {
            return null;
        }

        KeyValue ret = current;
        if(next_offset < max_offset) {
            current = BTUtils.getKeyValueAt(leaf_page, leaf_page.getInt(next_offset));
            next_offset += 4;
        } else {
            current = null;
        }

        return ret;
    }
}
