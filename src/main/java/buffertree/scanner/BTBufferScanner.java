package buffertree.scanner;


import buffertree.BTPage;
import buffertree.BTPageHeader;
import buffertree.BTUtils;
import buffertree.BTree;
import org.apache.hadoop.hbase.KeyValue;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by t-guje on 9/11/2015.
 */
public class BTBufferScanner implements IKeyValueScanner {

    BTree tree;
    KeyValue startKey;
    KeyValue stopKey;

    long page_id;
    ByteBuffer buffer_page;
    int[] offsets;
    int next_index;
    KeyValue current;

    public BTBufferScanner(BTree tree, long page_id, KeyValue startKey, KeyValue stopKey) {
        this.tree = tree;
        this.startKey = startKey;
        this.stopKey = stopKey;
        this.page_id = page_id;
        this.startKey = startKey;
        this.stopKey = stopKey;
        this.buffer_page = tree.readPage(page_id, BTPageHeader.Type.buffer);
        getValidItems();
        this.next_index = 0;
        next();
    }

    public BTBufferScanner(BTree tree, long page_id, ByteBuffer buffer_page, KeyValue startKey, KeyValue stopKey) {
        this.tree = tree;
        this.startKey = startKey;
        this.stopKey = stopKey;
        this.page_id = page_id;
        this.buffer_page = buffer_page;
        getValidItems();
        this.next_index = 0;
        next();
    }

    private boolean isValid(KeyValue kv) {
        boolean greaterThanEqualStart;
        boolean lesserThanStop;
        greaterThanEqualStart = KeyValue.COMPARATOR.compareRows(startKey, kv) <= 0;
        lesserThanStop = KeyValue.COMPARATOR.compareRows(kv, stopKey) <= 0;
        return greaterThanEqualStart && lesserThanStop;
    }

    //get the valid items and sort them
    private void getValidItems() {
        int offset = BTPageHeader.startOfFreeSpaceDefault;
        int max_offset = BTPageHeader.getStartOfFreeSpace(buffer_page);
        KeyValue temp = null;
        ArrayList<Integer> offsets_list = new ArrayList<Integer>();
        while(offset < max_offset) {
            temp = BTUtils.getKeyValueAt(buffer_page, offset);
            if(isValid(temp)) {
                offsets_list.add(offset);
            }
            offset += temp.getLength();
        }
        offsets = new int[offsets_list.size()];
        for(int i = 0; i < offsets_list.size(); i++) {
            offsets[i] = offsets_list.get(i);
        }
        BTPage.sortOffsets(offsets, buffer_page);
    }


    public KeyValue peek() {
        return current;
    }

    public KeyValue next() {

        //scanner is already done
        if(next_index > offsets.length) {
            return null;
        }

        //last element
        if(next_index == offsets.length) {
            next_index++;
            KeyValue ret = current;
            current = null;
            return ret;
        }

        KeyValue ret = current;
        current = BTUtils.getKeyValueAt(buffer_page, offsets[next_index]);
        next_index++;
        return ret;
    }
}
