package buffertree.scanner;

import buffertree.BTPage;
import buffertree.BTPageHeader;
import buffertree.BTUtils;
import buffertree.BTree;
import org.apache.hadoop.hbase.KeyValue;

import java.nio.ByteBuffer;

/**
 * Created by t-guje on 9/11/2015.
 */
public class BTNodeScanner implements IKeyValueScanner{

    BTree tree;
    KeyValue startKey;
    KeyValue stopKey;

    long page_id;
    ByteBuffer node_page;
    int max_offset;
    int current_offset;

    IKeyValueScanner child_scanner;
    IKeyValueScanner buffer_scanner;
    KeyValue next_child_kv;
    KeyValue next_buffer_kv;
    KeyValue current;

    public BTNodeScanner(BTree tree, long page_id, KeyValue startKey, KeyValue stopKey) {
        this.tree = tree;
        this.startKey = startKey;
        this.stopKey = stopKey;
        this.page_id = page_id;

        this.node_page = tree.readPage(page_id, BTPageHeader.Type.internal_node); //works for both leaf and internal node
        this.current_offset = BTPage.binSearchLessThanEquals(node_page, startKey);
        if(this.current_offset == -1) {
            this.current_offset = BTPageHeader.startOfFreeSpaceDefault;
        }
        this.max_offset = BTPage.binSearchGreaterThan(node_page, stopKey);
        if(this.max_offset == -1) {
            this.max_offset = BTPageHeader.getStartOfFreeSpace(node_page);
        }

        advanceChildCursor();

        this.buffer_scanner = new BTBufferScanner(tree, page_id + BTree.buffer_offset, startKey, stopKey);
        this.next_buffer_kv = this.buffer_scanner.peek();
        next();
    }


    public BTNodeScanner(BTree tree, long node_page_id, ByteBuffer node_page,
                                     long buffer_page_id, ByteBuffer buffer_page,
                                      KeyValue startKey, KeyValue stopKey) {

        this.tree = tree;
        this.startKey = startKey;
        this.stopKey = stopKey;
        this.page_id = node_page_id;

        this.node_page = node_page;
        this.current_offset = BTPage.binSearchLessThanEquals(node_page, startKey);
        if(this.current_offset == -1) {
            this.current_offset = BTPageHeader.startOfFreeSpaceDefault;
        }
        this.max_offset = BTPage.binSearchGreaterThan(node_page, stopKey);
        if(this.max_offset == -1) {
            this.max_offset = BTPageHeader.getStartOfFreeSpace(node_page);
        }

        advanceChildCursor();

        this.buffer_scanner = new BTBufferScanner(tree, buffer_page_id, buffer_page, startKey, stopKey);
        this.next_buffer_kv = this.buffer_scanner.peek();
        next();
    }

    private void advanceChildCursor() {
        if(child_scanner == null && current_offset >= max_offset) {
            next_child_kv = null;
        } else if(child_scanner != null) {
            child_scanner.next();
            next_child_kv = child_scanner.peek();
            if(next_child_kv == null) {
                boolean success = moveToNextChild();
                if(success) {
                    next_child_kv = child_scanner.peek();
                } else {
                    next_child_kv = null;
                }
            }
        } else if(moveToNextChild()) {
            next_child_kv = child_scanner.peek();
        }
    }

    private void advanceBufferCursor() {
        if(next_buffer_kv != null) {
            buffer_scanner.next();
            next_buffer_kv = buffer_scanner.peek();
        }
    }

    private boolean moveToNextChild() {
        if(current_offset < max_offset) {
            long child_id = BTUtils.getBlockIdFromIndex(BTUtils.getKeyValueAt(node_page, node_page.getInt(current_offset)));
            current_offset += 4;

            if(BTPageHeader.getPageType(node_page) == BTPageHeader.Type.internal_node) {
                child_scanner = new BTNodeScanner(tree, child_id, startKey, stopKey);
            } else {
                child_scanner = new BTLeafScanner(tree, child_id, startKey, stopKey);
            }
            return true;
        } else {
            child_scanner = null;
            return false;
        }
    }

    @Override
    public KeyValue peek() {
        return current;
    }

    @Override
    public KeyValue next() {

        boolean children_done = (next_child_kv == null);
        boolean buffer_done = (next_buffer_kv == null);
        boolean scanner_done = (current == null) && children_done && buffer_done;

        if(scanner_done) {
            return null;
        }

        KeyValue ret = current;
        if(children_done && buffer_done) {
            current = null;
        } else if(buffer_done) {
            current = next_child_kv;
            advanceChildCursor();
        } else if(children_done) {
            current = next_buffer_kv;
            advanceBufferCursor();
        } else {
            int cmp = KeyValue.COMPARATOR.compare(next_buffer_kv, next_child_kv);
            if(cmp < 0) {
                current = next_buffer_kv;
                advanceBufferCursor();
            } else {
                current = next_child_kv;
                advanceChildCursor();
            }
        }
        return ret;
    }
}
