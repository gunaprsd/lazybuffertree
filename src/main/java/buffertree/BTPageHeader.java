package buffertree;

import org.apache.hadoop.hbase.KeyValue;

import java.nio.ByteBuffer;

public class BTPageHeader {

	public static int startOfFreeSpaceDefault = 32;

	public enum Type {
		leaf((byte)0),
		leaf_node((byte)1),
		internal_node((byte)2),
		buffer((byte)3);
		
		byte type;
		Type(byte type) {
			this.type = type;
		}
		
		byte getType() {
			return type; 
		}
	}
	
	public static Type getPageType(ByteBuffer buffer) {
		byte type =  buffer.get(0);
		if(type == Type.leaf.getType())
			return Type.leaf; 
		else if(type == Type.leaf_node.getType())
			return Type.leaf_node;
		else if(type == Type.internal_node.getType())
			return Type.internal_node;
		else if(type == Type.buffer.getType())
			return Type.buffer;
		else {
			System.err.println("Incompatible type!");
			System.exit(0);
			return null;
		}
			
	}
	
	public static int getPageSize(ByteBuffer buffer) {
		if(buffer.capacity() == 0)
			return 0;
		return buffer.getInt(1);
	}
	
	public static int getStartOfFreeSpace(ByteBuffer buffer) {
		if(buffer.capacity() == 0)
			return 0;
		return buffer.getInt(5);
	}
	
	public static void setStartOfFreeSpace(ByteBuffer buffer, int value) {
		if(buffer.capacity() == 0)
			return;
		buffer.putInt(5, value);
	}
	
	public static int getEndOfFreeSpace(ByteBuffer buffer) {
		if(buffer.capacity() == 0)
			return 0;
		return buffer.getInt(9);
	}

	public static void setEndOfFreeSpace(ByteBuffer buffer, int value) {
		if(buffer.capacity() == 0)
			return;
		buffer.putInt(9, value);
	}

	public static long getPrevPageBlkNum(ByteBuffer buffer) {
		if(buffer.capacity() == 0)
			return -1;
		return buffer.getLong(13);
	}
	
	public static void setPrevPageBlkNum(ByteBuffer buffer, long value)  {
		if(buffer.capacity() == 0)
			return;
		buffer.putLong(13, value);
	}
	
	public static long getNextPageBlkNum(ByteBuffer buffer) {
		if(buffer.capacity() == 0)
			return 0;
		return buffer.getLong(21);
	}
	
	public static void setNextPageBlkNum(ByteBuffer buffer, long value)  {
		if(buffer.capacity() == 0)
			return;
		buffer.putLong(21, value);
	}
	
	public static long getNodeBlkNum(ByteBuffer buffer) {
		if(buffer.capacity() == 0)
			return 0;
		return buffer.getLong(9);
	}
	
	public static void setNodeBlkNum(ByteBuffer buffer, long value)  {
		if(buffer.capacity() == 0)
			return;
		buffer.putLong(9, value);
	}

	public static long getNextBufferBlkNum(ByteBuffer buffer) {
		if(buffer.capacity() == 0)
			return 0;
		return buffer.getLong(17);
	}
	
	public static void setNextBufferBlkNum(ByteBuffer buffer, long value)  {
		if(buffer.capacity() == 0)
			return;
		buffer.putLong(17, value);
	}

	public static void setPageHeaderDataTemplate(ByteBuffer buffer, 
												 Type type, 
												 int size,
												 long nodeBlockNum,
												 long prevBlockNum,
												 long nextBlockNum) {
		if(buffer.capacity() == 0)
			return;
		buffer.put(0, type.getType());
		buffer.putInt(1, size);
		buffer.putInt(5, startOfFreeSpaceDefault); //start of free space
		if(type == Type.buffer) {
			assert prevBlockNum == -1; 
			buffer.putLong(9, nodeBlockNum); 
			buffer.putLong(17, nextBlockNum);
		} else {
			assert nodeBlockNum == -1;
			buffer.putInt(9, size); //end of free space
			buffer.putLong(13, prevBlockNum); 
			buffer.putLong(21, nextBlockNum);
		}
	}

	public static int getFreeSpace(ByteBuffer buffer) {
		if(buffer.capacity() == 0) {
			return 0;
		}
		int startOfFreeSpace = getStartOfFreeSpace(buffer); 
		int endOfFreeSpace; 
		if(getPageType(buffer) == Type.buffer) {
			endOfFreeSpace = getPageSize(buffer);
		} else {
			endOfFreeSpace = getEndOfFreeSpace(buffer);
		}
		return (endOfFreeSpace - startOfFreeSpace);
	}
	
	public static int getCount(ByteBuffer page) {
		if(page.capacity() == 0)
			return 0;
		assert getPageType(page) != Type.buffer; 
		int startOfFreeSpace = getStartOfFreeSpace(page);
		return (startOfFreeSpace - startOfFreeSpaceDefault)/4 ; 
	}
	
	public static int getPageCapacity(ByteBuffer page) {
		if(page.capacity() == 0)
			return 0;
		return BTPageHeader.getPageSize(page) - BTPageHeader.startOfFreeSpaceDefault;
	}
	
	public static int getTupleCount(ByteBuffer page) {
		if(page.capacity() == 0)
			return 0;
		return (BTPageHeader.getStartOfFreeSpace(page) - BTPageHeader.startOfFreeSpaceDefault)/4;
	}
	
	public static int getDataSize(ByteBuffer page) {
		if(page.capacity() == 0)
			return 0;
		if(getPageType(page) == Type.buffer) {
			if(page.capacity() == 0) {
				return 0;
			}
			int startOfFreeSpace = BTPageHeader.getStartOfFreeSpace(page);
			return startOfFreeSpace - startOfFreeSpaceDefault; 
		} else {
			int endOfFreeSpace = BTPageHeader.getEndOfFreeSpace(page);
			int size = BTPageHeader.getPageSize(page);
			return size - endOfFreeSpace; 
		}
	}
	
	public static void cleanData(ByteBuffer page){
		if(page.capacity() == 0)
			return;
		page.putInt(5, startOfFreeSpaceDefault); //start of free space
		if(getPageType(page) != Type.buffer)
			page.putInt(9, getPageSize(page)); //end of free space
	}

	public static KeyValue getHighKey(ByteBuffer page) {
		if(page.capacity() == 0)
			return null;
		int offset = BTPageHeader.getStartOfFreeSpace(page);
		return BTUtils.getKeyValueAt(page, page.getInt(offset-4));
	}
	
}
