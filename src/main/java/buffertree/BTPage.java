package buffertree;

import org.apache.hadoop.hbase.KeyValue;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/*
 * This class contains operations that can be done on a particular 
 * page
 */
public class BTPage {
	
	/**
	 * Inserts the given key value into the page. Not necessarily in sorted order
	 * @param page
	 * @param kv
	 */
	public static void insert(ByteBuffer page, KeyValue kv) {
		//page might not have space
		int length = kv.getLength();
		int freeSpace = BTPageHeader.getFreeSpace(page);
		if((length + 4)> freeSpace) {
			System.err.println("No free space");
			System.exit(0);
		} else {
			int startOfFreeSpace = BTPageHeader.getStartOfFreeSpace(page);
			doInsert(page, kv, startOfFreeSpace);
		}
	}

	/**
	 * Rebuilds the index for the page. Not in sorted order.
	 * @param page
	 */
	public static void rebuildIndex(ByteBuffer page) {
		int endOfFreeSpace = BTPageHeader.getEndOfFreeSpace(page);
		int indexOffset = BTPageHeader.startOfFreeSpaceDefault;
		int dataOffset = endOfFreeSpace; 
		int size = BTPageHeader.getPageSize(page);
		page.position(dataOffset); 
		while(dataOffset < size) {
			page.putInt(indexOffset, dataOffset);
			indexOffset += 4;
			dataOffset += BTUtils.getKeyValueLengthAt(page, dataOffset);
		}
		BTPageHeader.setStartOfFreeSpace(page, indexOffset);
	}

    public static void sortOffsets(int[] offsets, ByteBuffer page) {
        for(int i = 0; i < offsets.length; i++) {
            for(int j = 1; j < offsets.length - i; j++) {
                KeyValue kv1 = BTUtils.getKeyValueAt(page, offsets[j-1]);
                KeyValue kv2 = BTUtils.getKeyValueAt(page, offsets[j]);
                int cmp = KeyValue.COMPARATOR.compare(kv1, kv2);
                if(cmp > 0) {
                    int off = offsets[j-1];
                    offsets[j-1] = offsets[j];
                    offsets[j] = off;
                }
            }
        }
    }
	/**
	 * Sorts the indices based on the key values using bubble sort
	 * @param page
	 */
	public static void sortIndex(ByteBuffer page) {
		int start_index = BTPageHeader.startOfFreeSpaceDefault;
		int end_index = BTPageHeader.getStartOfFreeSpace(page);
		int num_elems = BTPageHeader.getCount(page);
		
		for(int iterations = 0; iterations < num_elems; iterations++) {
			KeyValue kv1, kv2;
			for(int index = start_index; index < (end_index - 4 * (iterations + 1)); index += 4) { 
				kv1 = BTUtils.getKeyValueAt(page, page.getInt(index));
				kv2 = BTUtils.getKeyValueAt(page, page.getInt(index + 4));
				int cmp = KeyValue.COMPARATOR.compare(kv1, kv2);
				if(cmp > 0) {
					int temp = page.getInt(index + 4);
					page.putInt(index + 4, page.getInt(index)); 
					page.putInt(index, temp); 
				}
			}
		}
	}
	
	/**
	 * Inserts the given kv in the page at the offset and adds the offset at the index
	 * @param page
	 * @param kv
	 * @param offset
	 */
	private static void doInsert(ByteBuffer page, KeyValue kv, int offset) {
		int startOfFreeSpace = BTPageHeader.getStartOfFreeSpace(page);
		int endOfFreeSpace = BTPageHeader.getEndOfFreeSpace(page);
		
		endOfFreeSpace -= kv.getLength();
		page.position(endOfFreeSpace); 
		page.put(kv.getBuffer(), kv.getOffset(), kv.getLength());
		
		if(startOfFreeSpace != offset)
			System.arraycopy(page.array(), offset, page.array(), offset+4, startOfFreeSpace - offset);
		page.putInt(offset, endOfFreeSpace);
		startOfFreeSpace += 4; 
		
		BTPageHeader.setStartOfFreeSpace(page, startOfFreeSpace);
		BTPageHeader.setEndOfFreeSpace(page, endOfFreeSpace);
	}
	
	/**
	 * Inserts the given key value in the sorted page
	 * @param page
	 * @param kv
	 */
	public static void insertSorted(ByteBuffer page, KeyValue kv) {
		int length = kv.getLength();
		int freeSpace = BTPageHeader.getFreeSpace(page);
		if((length + 4)> freeSpace) {
			//should not occur
		} else {
			int offset = binSearchGreaterThan(page, kv);
			if(offset == -1) {
				offset = BTPageHeader.getStartOfFreeSpace(page);
			}
			doInsert(page, kv, offset);
		}
	}
	
	/**
	 * Returns the offset of key value that is greater than or equal to scan key
	 * @param page
	 * @param scanKey
	 * @return
	 */
	public static int binSearchEqual(ByteBuffer page, KeyValue scanKey) {
		int beg = BTPageHeader.startOfFreeSpaceDefault;
		int end = BTPageHeader.getStartOfFreeSpace(page) - 4;

		int tupleOffset = -1, cmp;
		while(beg <= end) {

			//retrieve key value
			int mid = (beg + end)/2 ;
			mid -= mid % 4;
			tupleOffset = page.getInt(mid);
			KeyValue midKey = BTUtils.getKeyValueAt(page, tupleOffset);

			cmp = KeyValue.COMPARATOR.compare(scanKey, midKey);

			if(cmp == 0) {
				return mid;
			} else if(cmp > 0) {
				beg = mid + 4;
			} else {
				end = mid - 4;
			}
		}
		return -1;
	}


	public static int binSearchGreaterThan(ByteBuffer page, KeyValue scanKey) {
		int beg = BTPageHeader.startOfFreeSpaceDefault;
		int end = BTPageHeader.getStartOfFreeSpace(page) - 4;

		int tupleOffset = -1, cmp;
		while(beg <= end) {

			//retrieve key value
			int mid = (beg + end)/2 ;
			mid -= mid % 4;
			tupleOffset = page.getInt(mid);
			KeyValue midKey = BTUtils.getKeyValueAt(page, tupleOffset);

			cmp = KeyValue.COMPARATOR.compare(scanKey, midKey);

			if(cmp >= 0) {
				beg = mid + 4;
			} else {
				end = mid - 4;
			}
		}

		if(beg >= BTPageHeader.getStartOfFreeSpace(page)) {
			return -1;
		} else {
			return beg;
		}
	}

	public static int binSearchGreaterThanEquals(ByteBuffer page, KeyValue scanKey) {
		int offset = BTPage.binSearchEqual(page, scanKey);
		if(offset == -1) {
			offset = BTPage.binSearchGreaterThan(page, scanKey);
			return offset;
		} else {
			return offset;
		}
	}

	public static int binSearchLessThanEquals(ByteBuffer page, KeyValue scanKey) {
		int beg = BTPageHeader.startOfFreeSpaceDefault;
		int end = BTPageHeader.getStartOfFreeSpace(page) - 4;

		int tupleOffset = -1, cmp;
		while(beg <= end) {

			//retrieve key value
			int mid = (beg + end)/2 ;
			mid -= mid % 4;
			tupleOffset = page.getInt(mid);
			KeyValue midKey = BTUtils.getKeyValueAt(page, tupleOffset);

			cmp = KeyValue.COMPARATOR.compareRows(scanKey, midKey);

			if(cmp == 0) {
				return mid;
			} else if(cmp > 0) {
				beg = mid + 4;
			} else {
				end = mid - 4;
			}
		}

		if(end < BTPageHeader.startOfFreeSpaceDefault) {
			return -1;
		} else {
			return end;
		}
	}
}
