package buffertree;

import com.google.common.hash.BloomFilter;
import org.apache.hadoop.hbase.KeyValue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Stack;

public class BTNode {
	
	public static class Iterator implements BTUtils.Iterator {
		ByteBuffer page; 
		int index; 
		int max_index; 
		KeyValue current; 
		
		public Iterator(ByteBuffer page) {
			this.page = page; 
			this.index = BTPageHeader.startOfFreeSpaceDefault;
			this.max_index = BTPageHeader.getStartOfFreeSpace(page);
			this.current = BTUtils.getKeyValueAt(page, page.getInt(index));
			this.index += 4;
		}

		public boolean hasNext() {
			return (current != null); 
		}


		public KeyValue peek() {
			return current; 
		}

		public KeyValue next() {
			KeyValue ret = current;
			
			if(current == null) {
				return null;
			}
			
			if(index < max_index) {
				current = BTUtils.getKeyValueAt(page, page.getInt(index));
				index += 4;
			} else {
				current = null;
			}
			 
			return ret; 
		}
		
	}
	
	/**
	 * Inserts the passed on new index tuples into the leaf_page and splits the node if necessary. If the node is split, it returns
	 * an array list of index tuples to be inserted into the parent node. Else, returns null.
	 * @param btree
	 * @param stack
	 * @param node_page
	 * @param new_indextuples
	 * @return
	 */
	public static ArrayList<KeyValue> insert(BTree btree, Stack<ByteBuffer> stack, long node_page_id, ByteBuffer node_page, ArrayList<KeyValue> new_indextuples) {
		ArrayList<KeyValue> siblings = null; 
		
		int count = new_indextuples.size();
		int size = BTUtils.computeSize(new_indextuples);

		if(BTPageHeader.getFreeSpace(node_page) > (size + 4*count)) {
			for(int i = 0; i < new_indextuples.size(); i++) {
				BTPage.insertSorted(node_page, new_indextuples.get(i));
			}
			
			btree.writePage(node_page_id, node_page);
		} else {
			siblings = splitInsert(btree, stack, node_page_id, node_page, new_indextuples, size);
		}
		
		return siblings;
	}
	
	/**
	 * Splits the node page, inserts the new_tuples and returns the new siblings that have been created. 
	 * @param btree
	 * @param stack
	 * @param node_page
	 * @param new_tuples
	 * @param new_tuples_size
	 * @return
	 */
	public static ArrayList<KeyValue> splitInsert(BTree btree, Stack<ByteBuffer> stack, long node_page_id, ByteBuffer node_page, ArrayList<KeyValue> new_tuples, int new_tuples_size) {
		ArrayList<KeyValue> siblings = null;
		
		//find the number of nodes needed
		int per_page_capacity = BTPageHeader.getPageCapacity(node_page);
		
		int total_count = new_tuples.size() + BTPageHeader.getCount(node_page);
		int data_size = new_tuples_size + BTPageHeader.getDataSize(node_page);
		int index_size= total_count * 4; 
		int total_size = data_size + index_size; 
		
		int num_nodes = 2;
		int min_node_size = data_size / 2;
		if( total_size >= 2 * per_page_capacity) {
			num_nodes = (int) Math.ceil((double)total_size / per_page_capacity);  
			min_node_size = (data_size / num_nodes);
		}
		
		//combine and find the split points
		int[] split_points = new int[num_nodes + 1];
		ByteBuffer combined = BTUtils.mergeForSplit(node_page, new_tuples, new_tuples_size, min_node_size, split_points);
 
		//do actual split
		siblings = BTUtils.splitNode(btree, combined, split_points, node_page_id, node_page);
		
		return siblings;
	}

	public static boolean verifyExistence(BTree tree, long node_page_id, ByteBuffer node_page, KeyValue kv) {
		long page_id = node_page_id;
		ByteBuffer page = node_page;
		while(true) {
			boolean isLeaf = BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf;

			if(isLeaf) {
				int kv_offset = BTPage.binSearchEqual(page, kv);
				if(kv_offset == -1) {
					return false;
				} else {
					return true;
				}
			}

			boolean isLeafNode = BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf_node;

			long node_buffer_page_id = page_id + BTree.buffer_offset;
			ByteBuffer node_buffer_page = tree.readPage(node_buffer_page_id, BTPageHeader.Type.buffer);
			boolean isInBuffer = BTBuffer.verifyExistence(tree, node_buffer_page_id, node_buffer_page, kv);

			if(isInBuffer == true) {
				return true;
			}

			int child_offset = BTPage.binSearchLessThanEquals(page, kv);
			if(child_offset == -1) {
				return false;
			}
			KeyValue childIndexTuple = BTUtils.getKeyValueAt(page, page.getInt(child_offset));
			if (BTPageHeader.getPageType(page) == BTPageHeader.Type.leaf_node) {
				try {
					BloomFilter bf = BTLeaf.getBloomFilterFromTuple(childIndexTuple);
					if (bf.mightContain(kv.getKey())) {
						page_id = BTUtils.getBlockIdFromIndex(childIndexTuple);
						page = tree.readPage(page_id, BTPageHeader.Type.leaf);
						continue;
					} else {
						return false;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				page_id = BTUtils.getBlockIdFromIndex(childIndexTuple);
				page = tree.readPage(page_id, BTPageHeader.Type.internal_node);
				continue;
			}
		}
	}
}
