package experiments;

import buffertree.BTree;
import org.apache.hadoop.hbase.KeyValue;

import java.util.Random;

/**
 * Created by t-guje on 9/12/2015.
 */
public class VerifyExistenceTest {
    public static void insert_and_verify(double test_size) {
        BTree tree = new BTree("treefile.dat", true);
        Random gen1 = new Random(156);

        for(double i = 0; i < test_size; i++) {
            KeyValue kv = TestUtils.generateRandomKeyValue(gen1);
            tree.insert(kv);
        }

        tree.close(true);
        tree = new BTree("treefile.dat", false);
        Random gen2 = new Random(156);
        for(double i = 0; i < test_size; i++) {
            if(i==120) {
                int a = 5;
            }
            KeyValue kv = TestUtils.generateRandomKeyValue(gen2);
            boolean exists = tree.verifyExistence(kv);
            if (!exists) throw new AssertionError();
        }

        tree.close();
    }

    public static void main(String[] args) {
        insert_and_verify(10000);
    }
}
