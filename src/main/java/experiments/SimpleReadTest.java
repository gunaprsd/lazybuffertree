package experiments;

import buffertree.BTree;
import buffertree.scanner.KeyValueScanner;
import org.apache.hadoop.hbase.KeyValue;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;

/**
 * Created by t-guje on 9/11/2015.
 */
public class SimpleReadTest {

    public static void insert_verify_immediate(double test_size) {
        BTree tree = new BTree("treefile.dat", true);
        Random gen1 = new Random(156);
        double count = 0;
        for(double i = 0; i < test_size; i++) {
            KeyValue kv = TestUtils.generateRandomKeyValue(gen1);
            tree.insert(kv);
            KeyValueScanner temp = tree.getKeyValue(kv);
            while (temp.peek() != null) {
                if (KeyValue.COMPARATOR.compare(kv, temp.peek()) != 0) throw new AssertionError();
                count++;
                temp.next();
            }
        }
        if (count != test_size) throw new AssertionError();
        tree.close();
    }

    public static void insert_all_verify(double test_size) {
        BTree tree = new BTree("treefile.dat", true);
        Random gen1 = new Random(156);

        for(double i = 0; i < test_size; i++) {
            KeyValue kv = TestUtils.generateRandomKeyValue(gen1);
            tree.insert(kv);
        }
        tree.close(true);
       tree = new BTree("treefile.dat", false);
        Random gen2 = new Random(156);
        double count = 0;
        for(double i = 0; i < test_size; i++) {
            KeyValue kv = TestUtils.generateRandomKeyValue(gen2);
            KeyValueScanner temp = tree.getKeyValue(kv);

            if (temp.peek() != null) {
                if (KeyValue.COMPARATOR.compareRows(kv, temp.peek()) != 0) throw new AssertionError();
                count++;
            } else {
                System.out.println(kv + " not found");
            }
        }
        if (count != test_size) throw new AssertionError();
        String treeFileName = "tree_print.dat";
        try {
            PrintStream filStream = new PrintStream(new FileOutputStream(treeFileName));
            System.setOut(filStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        tree.close();
    }

    public static void print_test(String treeFileName) {
        BTree tree = new BTree(treeFileName, false);
        tree.close(true);
    }

    public static void main(String[] args) {
        //insert_verify_immediate(100000);
        insert_all_verify(1000);
        //print_test("treefile.dat");
    }
}
