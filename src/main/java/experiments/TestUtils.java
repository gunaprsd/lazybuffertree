package experiments;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.hadoop.hbase.KeyValue;

import java.util.Random;

/**
 * Created by t-guje on 9/11/2015.
 */
public class TestUtils {
    public static KeyValue generateRandomKeyValue(Random gen) {

        String row = RandomStringUtils.random(10 + gen.nextInt(5), 0, 0, true, false, null, gen);
        String family = RandomStringUtils.random(10 + gen.nextInt(5), 0, 0, true, false, null, gen);
        String qual = RandomStringUtils.random(10 + gen.nextInt(5), 0, 0, true, false, null, gen);
        String key = row + family + qual;
        Integer val = gen.nextInt();
        KeyValue ret = new KeyValue(key.getBytes(), null, null, val.toString().getBytes());

        return ret;
    }
}
