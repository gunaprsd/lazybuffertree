package experiments;

import buffertree.BTree;
import buffertree.scanner.KeyValueScanner;
import org.apache.hadoop.hbase.KeyValue;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;

/**
 * Created by t-guje on 9/14/2015.
 */
public class InsertExperiment {

    static Random lookup_gen;
    static Random insert_gen;
    static Random inserted_tuple_gen;
    static Random operation_gen;
    static int seed = 123;
    static BTree tree;
    static PrintStream stats_file;
    //Workload initially loaded with 5MB of data.
    public static void load_initial_data(int test_size) {
        double initTime = System.currentTimeMillis();;
        double inserted_size = 0;
        int count = 0;
        while(inserted_size < test_size) {
            KeyValue kv = TestUtils.generateRandomKeyValue(insert_gen);
            tree.insert(kv);
            inserted_size += kv.getLength();
            count++;
        }
        System.out.format("Inserted %d tuples. Total size : %f MB\n", count, (double)inserted_size/1e6 );
        System.out.format("Total time taken  : " + (System.currentTimeMillis() - initTime));
    }

    public static  void perform_lookup() {
        KeyValue kv = TestUtils.generateRandomKeyValue(lookup_gen);
        KeyValueScanner temp = tree.getKeyValue(kv);
        while (temp.peek() != null) {
            if (KeyValue.COMPARATOR.compare(kv, temp.peek()) != 0) throw new AssertionError();
            temp.next();
        }
    }

    public static void perform_insert() {
        KeyValue kv = TestUtils.generateRandomKeyValue(insert_gen);
        tree.insert(kv);
    }

    public static void perform_verify_insert() {
        boolean lookup = inserted_tuple_gen.nextInt() % 1000 > 250 ;
        if(lookup) {
            KeyValue kv = TestUtils.generateRandomKeyValue(lookup_gen);
            tree.verifyInsert(kv);
        } else {
            KeyValue kv = TestUtils.generateRandomKeyValue(insert_gen);
            tree.verifyInsert(kv);
        }
    }

    public static void perform_operations(long num_operations, int ltu_percent) {
        double initTime = System.currentTimeMillis();;
        for(long i = 0; i < num_operations; i+=1000) {
            for(int j = 0; j < 1000; j++) {
                boolean lookup = (operation_gen.nextInt() % 100) < ltu_percent;
                if(lookup) {
                    perform_lookup();
                } else {
                    perform_verify_insert();
                }
            }
            stats_file.println(System.currentTimeMillis() - initTime);
            if(i%10000 == 0)System.out.println(i);
        }
    }

    public static void initialize_experiment() {
        lookup_gen = new Random(seed);
        insert_gen = new Random(seed);
        inserted_tuple_gen = new Random(seed+4);
        operation_gen = new Random(seed-4);
        try {
            stats_file = new PrintStream(new FileOutputStream("buffertree_stats.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static  void main(String[] args) {
        initialize_experiment();
        tree = new BTree("treefile.dat", true);
        load_initial_data(5000000);
        //perform_operations(30000000, 80);
        tree.close();
    }
}
