package experiments;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.hadoop.hbase.KeyValue;

import buffertree.BTree;

public class SimpleInsertTest {

    public static void main(String[] args) {
        BTree tree = new BTree("treefile.dat", true);
        Random gen = new Random(156);
        double initTime = System.currentTimeMillis();;
        for(int i = 0; i < 1000000; i++) {
            tree.insert(TestUtils.generateRandomKeyValue(gen));
            if(i % 200000 == 0) {
                System.out.println((i / (double)1000000)*100 + "% done!");
                System.out.println("Time Elapsed : " + (System.currentTimeMillis() - initTime) + " ms" );
            }
        }
        System.out.println("100% done!");
        System.out.println("Time Elapsed : " + (System.currentTimeMillis() - initTime) + " ms" );

        String treeFileName = "tree_print.dat";
        try {
            PrintStream filStream = new PrintStream(new FileOutputStream(treeFileName));
            System.setOut(filStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        tree.close();
    }

}
